﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace N5_Test_Api.Models
{
    public partial class N5TestContext : DbContext
    {
        public N5TestContext()
        {
        }

        public N5TestContext(DbContextOptions<N5TestContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Permission> Permissions { get; set; } = null!;
        public virtual DbSet<PermissionType> PermissionTypes { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

                IConfigurationRoot configuration = new ConfigurationBuilder()
                     .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                     .AddJsonFile($"appsettings.{environmentName}.json")
                     .Build();

                optionsBuilder.UseSqlServer(configuration.GetConnectionString("N5DB"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Permission>(entity =>
            {
                entity.Property(e => e.Id).HasComment("Unique ID");

                entity.Property(e => e.ApellidoEmpleado)
                    .HasMaxLength(150)
                    .IsUnicode(false)
                    .HasComment("Employee Surname");

                entity.Property(e => e.FechaPermiso)
                    .HasColumnType("date")
                    .HasComment("Permission granted on Date");

                entity.Property(e => e.NombreEmpleado)
                    .HasMaxLength(150)
                    .IsUnicode(false)
                    .HasComment("Employee Forename");

                entity.Property(e => e.TipoPermiso).HasComment("Permission Type");

                entity.HasOne(d => d.TipoPermisoNavigation)
                    .WithMany(p => p.Permissions)
                    .HasForeignKey(d => d.TipoPermiso)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Permissions_PermissionTypes");
            });

            modelBuilder.Entity<PermissionType>(entity =>
            {
                entity.Property(e => e.Id).HasComment("Unique ID");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("Permission Description");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
