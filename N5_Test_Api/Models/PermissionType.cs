﻿using System;
using System.Collections.Generic;

namespace N5_Test_Api.Models
{
    public partial class PermissionType
    {
        public PermissionType()
        {
            Permissions = new HashSet<Permission>();
        }

        /// <summary>
        /// Unique ID
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Permission Description
        /// </summary>
        public string Descripcion { get; set; } = null!;

        public virtual ICollection<Permission> Permissions { get; set; }
    }
}
