﻿using Microsoft.AspNetCore.Mvc;
using N5_Test_Api.DTO;
using N5_Test_Api.Models;

namespace N5_Test_Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PermissionTypesController : ControllerBase
    {
        N5TestContext db = new N5TestContext();
        String environment;
        private readonly IConfiguration Configuration;

        public PermissionTypesController(IConfiguration configuration)
        {
            Configuration = configuration;
            environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production";
        }

        [HttpGet(Name = "GetPermissionTypes")]
        public ActionResult<List<PermissionTypeDTO>> Get()
        {
            if (environment != "Production")
            {
                List<PermissionTypeDTO> types = (from c in db.PermissionTypes
                    select new PermissionTypeDTO()
                    {
                        Id = c.Id,
                        Descripcion = c.Descripcion,
                    }).ToList();

                return types;

            } else
            {
                List<PermissionTypeDTO> types = new List<PermissionTypeDTO>();

                types.Add(new PermissionTypeDTO() { Id = 1, Descripcion = "Matrimonio" });
                types.Add(new PermissionTypeDTO() { Id = 2, Descripcion = "Nacimiento" });
                types.Add(new PermissionTypeDTO() { Id = 3, Descripcion = "Fallecimiento" });

                return types;
            }
        }
    }
}
