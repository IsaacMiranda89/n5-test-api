using Microsoft.AspNetCore.Mvc;
using N5_Test_Api.DTO;
using N5_Test_Api.Models;
using Nest;

namespace N5_Test_Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PermissionsController : ControllerBase
    {
        private readonly IConfiguration Configuration;
        ElasticClient elasticClient;
        N5TestContext db = new N5TestContext();
        String environment;

        public PermissionsController(IConfiguration configuration, ElasticClient elasticClient)
        {
            Configuration = configuration;
            this.elasticClient = elasticClient;
            environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production";
        }

        [HttpPost(Name = "RequestPermission")]
        public ActionResult<ApiResponse> Post(PermissionObj permission)
        {
            try
            {
                // add to database

                Permission permissionDb = new Permission()
                {
                    NombreEmpleado = permission.NombreEmpleado,
                    ApellidoEmpleado = permission.ApellidoEmpleado,
                    FechaPermiso = permission.FechaPermiso,
                    TipoPermiso = permission.TipoPermiso,
                };

                if (environment != "Production")
                {
                    db.Permissions.Add(permissionDb);
                    db.SaveChanges();
                }

                // add to elasticsearch

                string elastic_index = Configuration.GetValue<string>("Elastic:listIndex");

                if (permissionDb.Id == 0)
                {
                    var resMax = elasticClient.Search<PermissionDTO>(s => s
                        .Index(elastic_index)
                        .Take(1)
                        .Aggregations(aggr =>
                            aggr.Max("get_max_id", max =>
                                max.Field(field => field.Id)
                            )
                        )
                    );

                    var highesRes = resMax.Documents.FirstOrDefault();
                    int highestId = highesRes != null ? resMax.Documents.First().Id + 1 : 1;
                    permissionDb.Id = highestId;
                }

                PermissionDTO dto = new PermissionDTO()
                {
                    Id = permissionDb.Id,
                    NombreEmpleado = permission.NombreEmpleado,
                    ApellidoEmpleado = permission.ApellidoEmpleado,
                    FechaPermiso = permission.FechaPermiso,
                    TipoPermiso = permission.TipoPermiso,
                };

                IndexResponse result = elasticClient.Index(dto, idx => idx.Index(elastic_index));

                // api response

                if (result.IsValid)
                {
                    return Ok(new ApiResponse() { status = "ok", message = "permission inserted" });
                }
                else
                {
                    return Ok(new ApiResponse() { status = "error", message = String.Format("permission cannot be inserted / {0}", result.DebugInformation) });
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new ApiResponse() { status = "error", message = String.Format("we have a error when try inserted a permission / {0} \n{1}", ex.Message, ex.InnerException?.Message) });
            }
        }

        [HttpPut(Name = "ModifyPermission")]
        public ActionResult<ApiResponse> Update(PermissionDTO permission)
        {
            try
            {
                if (environment != "Production")
                {
                    // update on db

                    Permission permissionDb = db.Permissions.FirstOrDefault(w => w.Id == permission.Id);

                    if (permissionDb == null)
                    {
                        return BadRequest(new ApiResponse() { status = "error", message = "permission not exist" });
                    }

                    permissionDb.NombreEmpleado = permission.NombreEmpleado;
                    permissionDb.ApellidoEmpleado = permission.ApellidoEmpleado;
                    permissionDb.FechaPermiso = permission.FechaPermiso;
                    permissionDb.TipoPermiso = permission.TipoPermiso;

                    db.Permissions.Update(permissionDb);
                    db.SaveChanges();
                }
                    

                // update on elasticsearch

                string elastic_index = Configuration.GetValue<string>("Elastic:listIndex");

                var getResponse = elasticClient.Get<PermissionDTO>(permission.Id, idx => idx.Index(elastic_index));

                if (getResponse.Found)
                {
                    var existingDocument = getResponse.Source;

                    existingDocument.NombreEmpleado = permission.NombreEmpleado;
                    existingDocument.ApellidoEmpleado = permission.ApellidoEmpleado;
                    existingDocument.FechaPermiso = permission.FechaPermiso;
                    existingDocument.TipoPermiso = permission.TipoPermiso;

                    var indexResponse = elasticClient.Index(existingDocument, idx => idx.Index(elastic_index));

                    if (indexResponse.IsValid)
                    {
                        return Ok(new ApiResponse() { status = "ok", message = "permission updated" });
                    }
                    else
                    {
                        return BadRequest(new ApiResponse() { status = "error", message = "permission cannot be updated" });
                    }
                }
                else
                {
                    return Ok(new ApiResponse() { status = "error", message = "permission not found" });
                }

               
            }
            catch
            {
                return BadRequest(new ApiResponse() { status = "error", message = "we have a error when try updated a permission" });
            }
        }

        [HttpGet(Name = "GetPermissions")]
        public IReadOnlyCollection<PermissionDTO> Get()
        {
            string elastic_index = Configuration.GetValue<string>("Elastic:listIndex");

            ISearchResponse<PermissionDTO> Results = elasticClient.Search<PermissionDTO>(
                s => s
                    .Index(elastic_index)
                    .Query(
                        q => q.MatchAll()
                    )
            );

            return Results.Documents;
        }
    }
}