using Elasticsearch.Net;
using Microsoft.EntityFrameworkCore;
using N5_Test_Api.Models;
using Nest;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

IConfigurationRoot env_configuration = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile($"appsettings.{environmentName}.json")
                .Build();

// elastic search

SingleNodeConnectionPool pool = new SingleNodeConnectionPool(new Uri(env_configuration.GetValue<string>("Elastic:Url")));
ConnectionSettings settings = new ConnectionSettings(pool)
    .ServerCertificateValidationCallback((sender, certificate, chain, sslPolicyErrors) => true)
    .BasicAuthentication(
        env_configuration.GetValue<string>("Elastic:User"), 
        env_configuration.GetValue<string>("Elastic:Password")
    )
    .DefaultIndex(env_configuration.GetValue<string>("Elastic:listIndex"));
ElasticClient client = new ElasticClient(settings);
builder.Services.AddSingleton(client);


// Database

builder.Services.AddDbContext<N5TestContext>(options =>
{
    options.UseSqlServer(env_configuration.GetConnectionString("N5DB"));
});

var app = builder.Build();

// Configure the HTTP request pipeline.
app.UseSwagger();
app.UseSwaggerUI();

app.UseAuthorization();

app.MapControllers();

app.UseCors(x => x
    .AllowAnyOrigin()
    .AllowAnyHeader()
    .AllowAnyMethod()
);

app.Run();