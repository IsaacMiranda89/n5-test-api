﻿namespace N5_Test_Api.DTO
{
    public class ApiResponse
    {
        public string status { get; set; }
        public string message { get; set; }
    }
}
