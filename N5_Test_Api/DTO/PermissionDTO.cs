﻿namespace N5_Test_Api.DTO
{
    public partial class PermissionDTO : PermissionObj
    {
        /// <summary>
        /// Unique ID
        /// </summary>
        public int Id { get; set; }
    }

    public partial class PermissionObj
    {
        /// <summary>
        /// Employee Forename
        /// </summary>
        public string NombreEmpleado { get; set; } = null!;
        /// <summary>
        /// Employee Surname
        /// </summary>
        public string ApellidoEmpleado { get; set; } = null!;
        /// <summary>
        /// Permission Type
        /// </summary>
        public int TipoPermiso { get; set; }
        /// <summary>
        /// Permission granted on Date
        /// </summary>
        public DateTime FechaPermiso { get; set; }
    }

    public partial class PermissionTypeDTO
    {
        /// <summary>
        /// Unique ID
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Permission Description
        /// </summary>
        public string Descripcion { get; set; } = null!;
    }
}
