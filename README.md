# N5 Test Api



## clona el repositorio

```
git clone https://gitlab.com/IsaacMiranda89/n5-test-api.git
```

## restaura la base de datos

```
N5Test.bak
```

## para generar la imagen docker

```
docker build -t n5_api .

docker network create elastic
#misma red de elasticsearch

docker run -d --net elastic -p 8080:80 --name n5-container n5_api
```